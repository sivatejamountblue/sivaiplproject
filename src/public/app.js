fetch('./output/matchesPerYear.json')
.then(response => response.json())
.then(data => generateGraph(data));

function generateGraph(data){
  Highcharts.chart('container1', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Matches Per Year'
      },
      xAxis: {
        categories: Object.keys(data)
      },
      yAxis: {
        title: {
          text: 'No of Matches'
        }
      },
      series: [{
        name: 'YEARS',
        data: Object.values(data)
    
      }]
    });
}

fetch('./output/runsConcededPerTeam.json')
.then(response => response.json())
.then(data1 => generateGraph1(data1));

function generateGraph1(data1){
  Highcharts.chart('container2', {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Extra Runs Conceded'
      },
      xAxis: {
        categories: Object.keys(data1)
      },
      yAxis: {
        title: {
          text: 'No of Matches'
        }
      },
      series: [{
        name: 'Teams',
        data: Object.values(data1)
    
      }]
    });
}
fetch('./output/economicalBowler.json')
.then(response => response.json())
.then(data3 => generateGraph2(data3));

function generateGraph2(data3){
  Highcharts.chart('container3', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Top 10 Economical Bowlers'
      },
      xAxis: {
        categories: Object.keys(data3)
      },
      yAxis: {
        title: {
          text: 'Economy'
        }
      },
      series: [{
        name: 'Bowlers',
        data: Object.values(data3)
    
      }]
    });
}
fetch('./output/matchesWonYear.json')
.then(response => response.json())
  .then(data4 => {
    let array = []
  for (let key in data4){
      let exper = Object.keys(data4[key])
      array = array.concat(exper)
  }
  let years = Array.from(new Set(array)).sort()
  let valuess = []
  let teams = Object.keys(data4);
  teams.forEach(team =>{
    let valueArray = [];
    years.forEach(value => {
      if(data4[team][value.toString()]){
        valueArray.push(data4[team][value.toString()])
      }
      else valueArray.push(0);
    });
    valuess.push({name: team, data: valueArray})
  })
    return generateGraph3(years, valuess)
  });

function generateGraph3(x,y){
  Highcharts.chart('container4', {
      chart: {
        type: 'line'
      },
      title: {
        text: 'Matches Won Per Year'
      },
      xAxis: {
        categories: x
      },
      yAxis: {
        title: {
          text: 'Matches'
        }
      },
      series: y
    });
}
