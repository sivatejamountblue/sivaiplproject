function iplConductYear(matches){
    let matchesPerYear = {}
    for (let i =0; i < matches.length; i++){
        
        let year = (matches[i].season)
        if (matchesPerYear[year]) {
            matchesPerYear[year] += 1
        }
        else {
            matchesPerYear[year] = 1
        }
        
    }
    return matchesPerYear;
}

module.exports.iplConductYear = iplConductYear;

function matchesWonPerYear(matches){
    let matchesPerYear = {}
    for (let i =0; i < matches.length; i++){
        let year = matches[i].season
        let winningTeam = matches[i].winner
        if (matchesPerYear[winningTeam]) {
            if (matchesPerYear[winningTeam][year]){
                matchesPerYear[winningTeam][year] +=1 
            }
            else {
                if (!matchesPerYear[winningTeam][year]){
                    matchesPerYear[winningTeam][year] =1 
                }
            }
            
        }
        else {
            let initialValue = [[year, 1]]
            matchesPerYear[winningTeam] = Object.fromEntries(initialValue)
        }
    }
    return matchesPerYear;
}

module.exports.matchesWonPerYear = matchesWonPerYear;

function extraRuns(matches, deliverie, year){
    let totalExtraRuns = {}
    let temporaryMatchData = {}
    for (let match of matches){
        let season = match.season;
        let id = match.id;
        temporaryMatchData[id] = season;
    }
    for (let delivery of deliverie){
        let matchId = delivery.match_id
        if (temporaryMatchData[matchId] == year ){
            if (totalExtraRuns[delivery.bowling_team]){
                totalExtraRuns[delivery.bowling_team] = totalExtraRuns[delivery.bowling_team] + parseFloat(delivery.extra_runs);

            }
            else {
                totalExtraRuns[delivery.bowling_team] = parseFloat(delivery.extra_runs)
            }
        }
    }
         
    
    return totalExtraRuns;
    
}

module.exports.extraRuns = extraRuns;

function economicalBowlers(matches, deliverie, year){
    let bowlers = {}
    let storedData = {}
    for (let match of matches){
        let seasonId = match.season;
        let iid = match.id;
        storedData[iid] = seasonId;
    }
    for (let delivery of deliverie){
        let matcchId = delivery.match_id

        if (storedData[matcchId] == year){
            if (delivery.wide_runs == 0 && delivery.noball_runs == 0){

                if (bowlers[delivery.bowler]){
                    bowlers[delivery.bowler].totalBalls += 1
                    bowlers[delivery.bowler].totalRuns += parseFloat(delivery.total_runs)
                    let noOfRun = bowlers[delivery.bowler].totalRuns;
                    let noOfBall = bowlers[delivery.bowler].totalBalls;
                     bowlers[delivery.bowler].economy = ((noOfRun*6)/noOfBall).toFixed(2)
    
                }
                else {

                     bowlers[delivery.bowler] = {"totalRuns": parseFloat(delivery.total_runs), "totalBalls": 1, "economy": 0}
                }
            }
        }
    }
         

let topten = []
for (let eachBowler in bowlers){
     topten.push(bowlers[eachBowler].economy)
}
topten.sort(function(a,b){
    return a-b;
})
let myobect = {}
for (let j=0; j < 10; j++){
    for(let bowler in bowlers){
        if (bowlers[bowler].economy == topten[j]){
            myobect[bowler] = parseFloat(topten[j])
        }
    }
}
return myobect
}
module.exports.economicalBowlers = economicalBowlers;

function tossAndWin(matches){
    let eachTeam = {}
    for (let k = 0; k < matches.length; k++){
        
        if (matches[k].toss_winner == matches[k].winner){
            let team = matches[k].winner
            if (eachTeam[team]){
                eachTeam[team] += 1
            }
            else {
                eachTeam[team] = 1
            }
        }
    }
    return eachTeam
}
module.exports.tossAndWin = tossAndWin;

function strikerate(bowling, matcheData){
    let overallStrikeRate = {} 
    let strikeintialData = {} 
    for (let matchh of matcheData){
        let id = matchh.id
        let year = matchh.season
        strikeintialData[id] = year
    } 
    for (let deliveryss of bowling){
        let deliveryMatchId = deliveryss.match_id
        let seasons = strikeintialData[deliveryss.match_id]
        if (strikeintialData[deliveryMatchId]){
            if (deliveryss.wide_runs == 0 && deliveryss.noball_runs == 0){
                if (overallStrikeRate[deliveryss.batsman]){
                    if (overallStrikeRate[deliveryss.batsman][seasons]){
                        overallStrikeRate[deliveryss.batsman][seasons].totalBalls += 1
                        overallStrikeRate[deliveryss.batsman][seasons].totalRuns += parseFloat(deliveryss.batsman_runs)
                        let noOfRuns = overallStrikeRate[deliveryss.batsman][seasons].totalRuns;
                        let noOfBalls = overallStrikeRate[deliveryss.batsman][seasons].totalBalls;
                        overallStrikeRate[deliveryss.batsman][seasons].strikeRate = ((noOfRuns*100)/noOfBalls).toFixed(2)
                    }
                    else {
                        overallStrikeRate[deliveryss.batsman][seasons] = {"totalRuns": parseFloat(deliveryss.batsman_runs), "totalBalls": 1, "strikeRate": 0}
                    }
                }
                        
                else{
                    
                    overallStrikeRate[deliveryss.batsman] = {}
                }  
            }
        }
    }
                
    for (let mainKey in overallStrikeRate){
        for (let subKey in overallStrikeRate[mainKey]){
            overallStrikeRate[mainKey][subKey] = overallStrikeRate[mainKey][subKey]['strikeRate']
        }
    }
    return overallStrikeRate
}
module.exports.strikerate = strikerate;



function playerOfMatch(matchesinfo){
    let playerMatch = {}
    for (let player of matchesinfo){
        if(playerMatch[player.season]){
           if (playerMatch[player.season][player.player_of_match]){
               playerMatch[player.season][player.player_of_match] +=1
           }
           else{
            playerMatch[player.season][player.player_of_match] =1
           }
        }
        else {
            let variab = [[player.player_of_match,1]]
            playerMatch[player.season] = Object.fromEntries(variab)
        }
    }
    
    for (let top in playerMatch){
        let sortedArray = Object.values(playerMatch[top]).sort((a,b) => b-a)[0]

        let result = {}
        for (const keey in playerMatch[top]) {
            if (sortedArray == playerMatch[top][keey]){
                result[keey] = sortedArray
            }
        }
        playerMatch[top] = result
        
    }
    return playerMatch
    

}
module.exports.playerOfMatch = playerOfMatch;

function mostDismissals(data){
   let dismissal = {}
   for (let eachDismisaal of data){
       if (dismissal[eachDismisaal.batsman]){
           if ((eachDismisaal.batsman === eachDismisaal.player_dismissed) && (eachDismisaal.dismissal_kind != "run out")&& (eachDismisaal.dismissal_kind != 'obstructing the field') && (eachDismisaal.dismissal_kind != 'retired')){
               if (dismissal[eachDismisaal.batsman][eachDismisaal.bowler]){
                dismissal[eachDismisaal.batsman][eachDismisaal.bowler] += 1
               }
               else {
                dismissal[eachDismisaal.batsman][eachDismisaal.bowler] = 1
               }
           }
       }
       else {
           let bowl = [[eachDismisaal.bowler, 1]]
           dismissal[eachDismisaal.batsman] = Object.fromEntries(bowl)
       }
   }
   for (let first in dismissal){
    let desc = Object.values(dismissal[first]).sort((a,b) => b-a)[0]

    let resultss = {}
    for (const name in dismissal[first]) {
        if (desc == dismissal[first][name]){
            resultss[name] = desc
        }
    }
    dismissal[first] = resultss
    
}
return dismissal
   
}
module.exports.mostDismissals = mostDismissals

function superOverBowler(details){
    let superOver = {}
    for (let ball of details){
        if (ball.is_super_over != 0){ 
            if (superOver[ball.bowler]){
                superOver[ball.bowler].totalBall += 1
                superOver[ball.bowler].totalRun += parseFloat(ball.total_runs)
                let Runs = superOver[ball.bowler].totalRun;
                let Balls = superOver[ball.bowler].totalBall;
                superOver[ball.bowler].economyOfBowler = ((Runs*6)/Balls).toFixed(2)
                }
            else {
                superOver[ball.bowler] = {"totalRun": parseFloat(ball.total_runs), "totalBall": 1, "economyOfBowler": 0}
            }
        
            
        }
    }
    let array = []
    for (let valu in superOver){
        array.push(superOver[valu].economyOfBowler)  
    }
    array.sort(function(a,b){
        return a-b;
    })
    let myobject = {}
    for (let c=0; c < 1; c++){
        for(let bowl in superOver){
            if (superOver[bowl].economyOfBowler == array[c]){
                myobject[bowl] = array[c]
            }
        }
    }
    return myobject
    }
module.exports.superOverBowler = superOverBowler;



function matchesWonPerTeam(match)                     //arr = [{}, {}, {}]
{
  var result={};                                   //result= { year1: {t1:  , t2:  ,t3: },  year2: {t1:  ,t2:  , t3: }}

  for(var i=0;i<match.length;i++)
  {
    if(result.hasOwnProperty(match[i].season))
    {
      if(result[match[i].season][match[i].winner]) //season+winner both exist
      {
        result[match[i].season][match[i].winner]+=1;
      }

      else //season exist,winner don't
      {
        result[match[i].season][match[i].winner]=1;
      }
    }

    else   //season dont exist
    {
         //result[match[i].season] = {match[i].winner :1} ;
        result[match[i].season] = Object.fromEntries([[match[i].winner, 1]]);
    }
  } 

  return result;

}

module.exports.matchesWonPerTeam=matchesWonPerTeam;