const csvtojson = require('csvtojson');
const fs = require('fs');
const path = require("path");

const csvFilePath = path.resolve(__dirname, "../data/matches.csv")
const csvFilePath1 = path.resolve(__dirname, "../data/deliveries.csv")

const {iplConductYear, matchesWonPerYear, extraRuns, economicalBowlers, tossAndWin, strikerate, playerOfMatch,mostDismissals,superOverBowler,matchesWonPerTeam }= require("./ipl.js")
const ipltest = async() => {
    const matchesData = await csvtojson().fromFile(csvFilePath).then((data)=>{return data});
    const deliveryData = await csvtojson().fromFile(csvFilePath1).then((data)=>{return data});
    let matchesPerYear = iplConductYear(matchesData);
    let matchesWonYear = matchesWonPerYear(matchesData);
    let runsConcededPerTeam = extraRuns(matchesData, deliveryData, 2016);
    let economicalBowler = economicalBowlers(matchesData, deliveryData, 2015);
    let sameTeamWin = tossAndWin(matchesData);
    let strikerateOfPlayer = strikerate(deliveryData, matchesData);
    let mostManOfMtaches = playerOfMatch(matchesData);
    let mostDismissedByBowler = mostDismissals(deliveryData);
    let economicalSuperOverBowler = superOverBowler(deliveryData);
    let matchesWonPerTea = matchesWonPerTeam(matchesData);
    console.log(matchesWonPerTea)
    

    
    writeFile(path.resolve(__dirname, "../public/output/matchesPerYear.json"), JSON.stringify(matchesPerYear, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/matchesWonYear.json"), JSON.stringify(matchesWonYear, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/runsConcededPerTeam.json"), JSON.stringify(runsConcededPerTeam, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/economicalBowler.json"), JSON.stringify(economicalBowler, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/sameTeamWin.json"), JSON.stringify(sameTeamWin, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/strikerateOfPlayer.json"), JSON.stringify(strikerateOfPlayer, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/mostManOfMtaches.json"), JSON.stringify(mostManOfMtaches, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/mostDismissedByBowler.json"), JSON.stringify(mostDismissedByBowler, null, 2));
    writeFile(path.resolve(__dirname, "../public/output/economicalSuperOverBowler.json"), JSON.stringify(economicalSuperOverBowler, null, 2));

}
ipltest()
function writeFile(path, deliverydata) {
    fs.writeFile(path, deliverydata, function(err){
        if (err){
            console.log(err);
        }
    })
}

//.........................................................................................................................................................//
